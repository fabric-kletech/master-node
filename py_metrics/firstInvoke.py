import os
import time
import subprocess

chaincodeName = os.environ.get('CHAINCODE_NAME')
incrementValue = 1
policyOrgs = int(os.environ.get('POLICY_ORGS'))
def createArg():
  Invoke_Arg = 'peer chaincode invoke -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n ' + chaincodeName + ' '
  no_of_orgs = int(os.environ.get('ORGS'))
  for i in range(1,policyOrgs+1):
    Invoke_Arg+=' --peerAddresses peer0.org'+str(i)+'.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org'+str(i)+'.example.com/peers/peer0.org'+str(i)+'.example.com/tls/ca.crt'
  Invoke_Arg+= ' -c \'{"Args":["invoke","a","b", "%d"]}\' ' % incrementValue
  print(Invoke_Arg)
  return Invoke_Arg

def query():
  cmd = 'peer chaincode query -C mychannel -n ' + chaincodeName + ' -c \'{"Args":["query","a"]}\''
  result = int(subprocess.check_output(cmd, shell=True))
  return result

def invoke(x):
  getCur = query()
  os.system(x)
  while True:
    x = query()
    if(x == (getCur-incrementValue)):
      break

if __name__=="__main__":
  invk = createArg()
  start = time.time()
  invoke(invk)
  end = time.time()
  print("-------\nInvoke Time" , end - start , "\n-------")
