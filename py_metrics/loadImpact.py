import os
import time
import subprocess

def createArg(val):
  Invoke_Arg = 'peer chaincode invoke -o orderer0.example.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n mycc '
  no_of_orgs = int(os.environ.get('ORGS'))
  for i in range(1,no_of_orgs+1):
    Invoke_Arg+=' --peerAddresses peer0.org'+str(i)+'.example.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org'+str(i)+'.example.com/peers/peer0.org'+str(i)+'.example.com/tls/ca.crt'
  Invoke_Arg+= ' -c \'{"Args":["invoke","a","b", "%d"]}\' ' % val
  return Invoke_Arg

def query():
  cmd = 'peer chaincode query -C mychannel -n mycc -c \'{"Args":["query","a"]}\''
  result = int(subprocess.check_output(cmd, shell=True))
  return result

def invoke(x):
  getCur = query()
  incrementValue = 1
  os.system(x)
  while True:
    x = query()
    if(x == (getCur-incrementValue)):
      break

if __name__=="__main__":
  invk = createArg(1)
  f = open("results/varying_loadQuery.csv" , 'w')
  f.write("No_of_transactions,Time(S)\n")
  cntr = 0
  time_t = 0.0
  for _ in range(5):
    start = time.time()
    # 25 , 50 , 75 , 100 ,125
    for _ in range (25):
      cntr+=1
      #invoke(invk)
      query()
    end = time.time()
    time_t += end-start
    f.write(str(cntr) + "," + str(time_t)+"\n")
    print(query())
  f.close()
