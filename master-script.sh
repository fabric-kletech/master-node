#!/bin/bash
source /tmp/multi/env.sh

function printHelp() {
  echo "Usage: "
  echo "  network_start.sh <mode> [-i] [-s] [-g]"
  echo "    <mode> - one of 'up', 'generate', 'init', 'swarm'"
  echo "      - 'up' - start the network on all the nodes"
  echo "      - 'down' - teardown network on all the nodes"
  echo "      - 'generate' - generates certificates"
  echo "      - 'init' - initialize all nodes"
  echo "      - 'swarm' - create swarm and join all nodes"
  echo "    -g - generate new certificates before starting the network "
  echo "    -i - initialize the network on all nodes before starting the network"
  echo "    -s - create new swarm network and join all the nodes before starting the network"
  echo "  network_start.sh -h (print this message)"
}

function stopNetwork() {
  for NODE in `seq 1 $NODES`
  do
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    ssh ubuntu@${!NODE_IP} 'bash -s' < network.sh down $NODE > log/log_down.txt
  done
}

function tearDownNetwork() {
  echo "Tearing down the network"
  for NODE in `seq 1 $NODES`
  do
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    echo "Initialized the network on Node$NODE"
    ssh ubuntu@${!NODE_IP} 'bash -s' < network.sh down $NODE > log/log_down.txt
    ssh ubuntu@${!NODE_IP} 'bash -s' < init.sh $NODE
  done
}

function transferConf() {
  cp ./compose-files/crypto-config.yaml ./compose-files/configtx.yaml .
  for NODE in `seq 1 $NODES`
  do
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    ssh ubuntu@${!NODE_IP} "mkdir /tmp/multi/" 2>log/log_init.txt
    scp -q /tmp/multi/env.sh ubuntu@${!NODE_IP}:/tmp/multi/
  done
}

function initializeNodes() {
  for NODE in `seq 1 $NODES`
  do
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    echo "Initialized the network on Node$NODE"
    ssh ubuntu@${!NODE_IP} 'bash -s' < network.sh down $NODE > log/log_down.txt
    ssh ubuntu@${!NODE_IP} 'bash -s' < init.sh $NODE > log/log_init.txt
  done
}

function joinSwarmNetwork() {
  docker swarm leave --force > log/log_swarm.txt
  echo "Joining Node1 to swarm network"
  docker swarm init --advertise-addr ${NODE1_IP} >> log/log_swarm.txt
  docker swarm join-token manager > token.txt
  grep docker token.txt > token.sh

  for NODE in `seq 2 $NODES`
  do
    echo "Joining Node$NODE to swarm network"
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    ex -sc "s/$/ --advertise-addr ${!NODE_IP} /|w|q" token.sh
    scp -q token.sh ubuntu@${!NODE_IP}:/tmp/multi
    rm token.sh
    ssh ubuntu@${!NODE_IP} 'bash -s' < join_token.sh >> log/log_swarm.txt
    grep docker token.txt > token.sh
  done
  rm token.txt token.sh
  docker network create --attachable --driver overlay $SWARM_NETWORK >> log/log_swarm.txt
}

function generateCertTransfer() {
  ./network.sh generate 2>&1 log/log_cryptogen.txt

  for NODE in `seq 2 $NODES`
  do
    echo "Transfering certificates to Node$NODE"
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    scp -q -r crypto-config channel-artifacts ubuntu@${!NODE_IP}:${!NODE_PATH} >> log/log_gen${NODE}.txt
  done
}

function startNetwork() {
  if [ "${GENERATE_CERTS}" == "true" ]; then
    initializeNodes
    transferConf
    joinSwarmNetwork
    generateCertTransfer
  elif [ "${NEW_SWARM}" == "true" ]; then
    stopNetwork
    joinSwarmNetwork
  fi
  for NODE in `seq 1 $NODES`
  do
    echo "---------Starting network on Node$NODE-----------"
    NODE_IP="NODE${NODE}_IP"
    NODE_PATH="FABRIC_CFG_PATH$NODE"
    network.sh up $NODE > log/log_up${NODE}.txt &
  done
  wait
}
MODE=$1
shift
ROLE=$2

GENERATE_CERTS=false
NEW_SWARM=false
IF_COUCHDB=false
CERTIFICATE_AUTHORITIES=false

while getopts "hgsda" opt; do
  case "$opt" in
  h)
    printHelp
    exit 0
    ;;
  g)
    GENERATE_CERTS=true
    ;;
  s)
    NEW_SWARM=true
    ;;
  d)
    IF_COUCHDB=true
    ;;
  a)
    CERTIFICATE_AUTHORITIES=true
    ;;
  esac
done


if [ "${MODE}" == "init" ]; then
  initializeNodes
elif [ "${MODE}" == "swarm" ]; then
  joinSwarmNetwork
elif [ "${MODE}" == "generate" ]; then
  generateCertTransfer
elif [ "${MODE}" == "up" ]; then
  startNetwork
elif [ "${MODE}" == "down" ]; then
  stopNetwork
elif [ "${MODE}" == "teardown" ]; then
  tearDownNetwork
elif [ "${MODE}" == "transfer" ]; then
  transferConf
else
  printHelp
  exit 1
fi

