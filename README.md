# Master-Node-Automation

## Description:
An Automated Multi Node repository that automates the entire process of building the Multi-Node setup by creating your own custom network configurations such as the number of nodes in the network, number of peers on each node, and some more details about the network, generate the certificates, transfers the generated certificates to all the nodes, creates a swarm network to connect docker containers on all the nodes and starts the network on all the nodes.

## Starting the network
1. Clone the Master-Node repository into the master node using **git clone git@gitlab.com:fabric-kletech/master-node.git**
2. Clone the Slave-Node repository into all the slave node using **git clone git@gitlab.com:fabric-kletech/slave-node.git** 
3. Make sure that the **bin** directory containing the crypto-gen tools is present at the correct location in master node.
4. Run the master-script.py and fill in the network details such as number of nodes, number of peers on each node, ip address of each node, hostname of each node, and working directory of each node.
5. The network_start.sh creates swarm network, joins all the nodes to the created swarm network, generate the certificates, transfer the certificates to all the nodes, and start the network on all the nodes.
