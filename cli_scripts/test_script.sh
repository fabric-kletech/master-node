source env.sh
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer0.example.com/msp/tls$
for ORG in `seq 1 $ORGS`;
do
  export PEER0_ORG${ORG}_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org${ORG}.example.com/peers/peer0.org${ORG}.example.com/tls/ca.crt
done
echo "$ORGS"

PEER=2
ORG=4
if (( $ORG<=$ORGS & $ORG>0 )); then
  CORE_PEER_LOCALMSPID="Org${ORG}MSP"
  TLS_PEER="PEER0_ORG${ORG}_CA"
  echo "${!TLS_PEER}"
  CORE_PEER_TLS_ROOTCERT_FILE="${!TLS_PEER}"
  CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org${ORG}.example.com/users/Admin@org${ORG}.example.com/msp
  NO_OF_PEERS="ORG${ORG}_PEERS"
#  echo "${!NO_OF_PEERS}"
  if (( $PEER>=0 & $PEER<"${!NO_OF_PEERS}" ))
  then
    CORE_PEER_ADDRESS=peer${PEER}.org${ORG}.example.com:7051
  else
    CORE_PEER_ADDRESS=peer0.org${ORG}.example.com:7051
    echo "================== WARNING !!! PEER Unknown =================="
  fi
  echo "$CORE_PEER_LOCALMSPID"
  echo "$CORE_PEER_TLS_ROOTCERT_FILE"
  echo "$CORE_PEER_MSPCONFIGPATH"
  echo "$CORE_PEER_ADDRESS"
else
  echo "================== ERROR !!! ORG Unknown =================="
fi
if [ "$VERBOSE" == "true" ]; then
  env | grep CORE
fi

POLICY="AND"
arg=$POLICY" ('Org1MSP.peer'"

for ORG in `seq 2 $ORGS`
do
  export temp=",'Org"${ORG}"MSP.peer'"
  echo $temp
  arg=${arg}${temp}
done
arg=${arg}")"

echo $arg
