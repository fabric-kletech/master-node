#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your Multi network (BYMN) end-to-end test"
echo
source env.sh
source scripts/ccEnv.sh
MODE="$1"
CHAINCODE_NAME="$2"
: ${MODE:="create"}
: ${CHAINCODE_NAME:="mycc"}

LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=10

CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/node"
fi

if [ "$LANGUAGE" = "java" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/java/"
fi

echo "Channel name : "$CHANNEL_NAME

# import utils
. scripts/utils.sh

createChannel() {
	setGlobals 0 1

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer0.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx >&log.txt
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer0.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel '$CHANNEL_NAME' created ===================== "
	echo
}

joinChannel () {
	for ORG in `seq 1 $ORGS`; do
	NO_OF_PEERS="ORG${ORG}_PEERS"
        x=$(( ${!NO_OF_PEERS}-1 ))
        for PEER in `seq 0 ${x}`; do
		joinChannelWithRetry $PEER $ORG
		echo "===================== peer${PEER}.org${ORG} joined channel '$CHANNEL_NAME' ===================== "
		sleep $DELAY
		echo
	    done
	done
}

function createJoinUpdateChannel() {
  ## Create channel
  echo "Creating channel ${CHANNEL_NAME}..."
  createChannel

  ## Join all the peers to the channel
  echo "Having all peers join the channel ${CHANNEL_NAME}..."
  joinChannel

  ## Set the anchor peers for each org in the channel ${CHANNEL_NAME}
  for ORG in `seq 1 $ORGS`; do
    echo "Updating anchor peers for org${ORG} in the channel ${CHANNEL_NAME}..."
    updateAnchorPeers 0 $ORG
  done

  #install and instantiate chaincode ${CHAINCODE_NAME} on channel ${CHANNEL_NAME}
  installInstantiateChaincode
}


function installInstantiateChaincode() {
  if [ "${NO_CHAINCODE}" != "true" ]; then
	## Install chaincode ${CHAINCODE_NAME} on all the peers on all orgs
	for ORG in `seq 1 $ORGS`; do
	  echo "Installing chaincode ${CHAINCODE_NAME} on peer0.org${ORG}..."
	  installChaincode 0 $ORG
	done

	# Instantiate chaincode ${CHAINCODE_NAME} on peer0.org1
	echo "Instantiating chaincode ${CHAINCODE_NAME} on peer0.org1..."
	instantiateChaincode 0 1
  fi
}

if [ "${MODE}" == "create" ]; then
  createJoinUpdateChannel
elif [ "${MODE}" == "install" ]; then
  installInstantiateChaincode
else
  #printHelp
  exit 1
fi

echo
echo "========= All GOOD, execution completed =========== "
echo
echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

exit 0
