#!/bin/bash
source /tmp/multi/env.sh

function clearContainers() {
  CONTAINER_IDS=$(docker ps -a | awk '($2 ~ /dev-peer.*.mycc.*/) {print $1}')
  if [ -z "$CONTAINER_IDS" -o "$CONTAINER_IDS" == " " ]; then
    echo "---- No containers available for deletion ----"
  else
    docker rm -f $CONTAINER_IDS
  fi
}

function removeUnwantedImages() {
  DOCKER_IMAGE_IDS=$(docker images | awk '($1 ~ /dev-peer.*.mycc.*/) {print $3}')
  if [ -z "$DOCKER_IMAGE_IDS" -o "$DOCKER_IMAGE_IDS" == " " ]; then
    echo "---- No images available for deletion ----"
  else
    docker rmi -f $DOCKER_IMAGE_IDS
  fi
}

function networkDown() {
  NODE=$1
  if [ $NODE -eq 1 ]; then
    echo "Clearing stack..."
    docker stack rm $DOCKER_STACK 2>&1 /dev/null
    docker run -v ${FABRIC_CFG_PATH1}:/tmp/fabric-multi-network --rm hyperledger/fabric-tools:$IMAGE_TAG rm -Rf /tmp/first-network/ledgers-backup
  fi
  echo "Clearing containers..."
  clearContainers
  echo "Clearing unwanted images..."
  removeUnwantedImages
  echo "Clearing volumes..."
  docker system prune -f
  docker volume prune -f
  if [[ $(docker ps -aq) != "" ]]; then
    docker rm $(docker ps -aq) -f
  fi
  if [[ $(docker volume ls) != "" ]]; then
    docker volume rm $(docker volume ls) -f
  fi
}


function networkUp() {
  NODE=$1
  NODE_IP="NODE${NODE}_IP"
  NODE_PATH="FABRIC_CFG_PATH$NODE"
  if [ $NODE -eq 1 ]; then
    if [ ! -d "${FABRIC_CFG_PATH1}/crypto-config" ]; then
      generateCerts
      replacePrivateKey
      generateChannelArtifacts
    fi
    startDockerServices ./compose-files/orderer
  fi
  startDockerServices ./compose-files/org${NODE}
  echo "Installed all required services"
}

function startDockerServices(){
  search_dir=$1
  for entry in "$search_dir"/*
    do
       docker stack deploy --compose-file $entry $DOCKER_STACK 2>&1
       if [ $? -ne 0 ]; then
         echo "ERROR !!!! Unable to start network"
         exit 1
       fi
    done
}

function execCli() {
 docker exec -it $(docker ps --filter name="cli" --format "{{.ID}}") bash
}

function replacePrivateKey() {
  ARCH=$(uname -s | grep Darwin)
  if [ "$ARCH" == "Darwin" ]; then
    OPTS="-it"
  else
    OPTS="-i"
  fi
  CURRENT_DIR=${FABRIC_CFG_PATH1}
  TEMPLATE_DIR=${FABRIC_CFG_PATH1}/compose-files/templates
  CA_SCRIPTS_DIR=${FABRIC_CFG_PATH1}/compose-files

  echo "Replacing Keys for all the ${ORGS} Nodes"
  for ORG in `seq 1 $ORGS`;
  do
    cp $TEMPLATE_DIR/docker-compose-ca${ORG}-template.yaml compose-files/org${ORG}/docker-compose-ca${ORG}.yaml

    cd crypto-config/peerOrganizations/org${ORG}.example.com/ca/
    PRIV_KEY=$(ls *_sk)
    cd "$CURRENT_DIR"
    sed $OPTS "s/CA${ORG}_PRIVATE_KEY/${PRIV_KEY}/g" compose-files/org${ORG}/docker-compose-ca${ORG}.yaml
  done
}

function generateCerts() {
  which cryptogen
  if [ "$?" -ne 0 ]; then
    echo "cryptogen tool not found. exiting"
    exit 1
  fi
  echo
  echo "##########################################################"
  echo "##### Generate certificates using cryptogen tool #########"
  echo "##########################################################"

  if [ -d "${FABRIC_CFG_PATH1}/crypto-config" ]; then
    rm -Rf ${FABRIC_CFG_PATH1}/crypto-config
  fi
  set -x
  cryptogen generate --config=${FABRIC_CFG_PATH1}/crypto-config.yaml
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate certificates..."
    exit 1
  fi
#  echo "Generate CCP files for Org1 and Org2"
#  ./ccp-generate.sh
}

function generateChannelArtifacts() {
  which configtxgen
  if [ "$?" -ne 0 ]; then
    echo "configtxgen tool not found. exiting"
    exit 1
  fi

  echo "##########################################################"
  echo "#########  Generating Orderer Genesis block ##############"
  echo "##########################################################"
  echo "CONSENSUS_TYPE="$CONSENSUS_TYPE
  set -x
  if [ "$CONSENSUS_TYPE" == "solo" ]; then
    configtxgen -profile MultiOrgsOrdererGenesis -channelID $SYS_CHANNEL -outputBlock ${FABRIC_CFG_PATH1}/channel-artifacts/genesis.block
  elif [ "$CONSENSUS_TYPE" == "kafka" ]; then
    configtxgen -profile SampleDevModeKafka -channelID $SYS_CHANNEL -outputBlock ${FABRIC_CFG_PATH1}/channel-artifacts/genesis.block
  elif [ "$CONSENSUS_TYPE" == "etcdraft" ]; then
    configtxgen -profile SampleMultiNodeEtcdRaft -channelID $SYS_CHANNEL -outputBlock ${FABRIC_CFG_PATH1}/channel-artifacts/genesis.block
  else
    set +x
    echo "unrecognized CONSESUS_TYPE='$CONSENSUS_TYPE'. exiting"
    exit 1
  fi
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate orderer genesis block..."
    exit 1
  fi
  echo
  echo "#################################################################"
  echo "### Generating channel configuration transaction 'channel.tx' ###"
  echo "#################################################################"
  set -x
  configtxgen -profile MultiOrgsChannel -outputCreateChannelTx ${FABRIC_CFG_PATH1}/channel-artifacts/channel.tx -channelID $CHANNEL_NAME
  res=$?
  set +x
  if [ $res -ne 0 ]; then
    echo "Failed to generate channel configuration transaction..."
    exit 1
  fi

  for ORG in `seq 1 $ORGS`;
  do
    echo
    echo "#################################################################"
    echo "#######    Generating anchor peer update for Org${ORG}MSP   ##########"
    echo "#################################################################"
    set -x
    configtxgen -profile MultiOrgsChannel -outputAnchorPeersUpdate ${FABRIC_CFG_PATH1}/channel-artifacts/Org${ORG}MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org${ORG}MSP
    res=$?
    set +x
    if [ $res -ne 0 ]; then
      echo "Failed to generate anchor peer update for Org${ORG}MSP..."
      exit 1
    fi
  done
}
MODE=$1
NODE=$2

if [ "${MODE}" == "up" ]; then
  networkUp $NODE
elif [ "${MODE}" == "down" ]; then
  networkDown $NODE
elif [ "${MODE}" == "generate" ]; then
  generateCerts
  replacePrivateKey
  generateChannelArtifacts
elif [ "${MODE}" == "exec" ]; then ## Exec Cli
  execCli
else
  printHelp
  exit 1
fi

