cd generator-peer
python3 input.py
python3 composeGen.py
python3 environment/env.py
cp env.sh ..
cp env.sh /tmp/multi/
cd ..
if [ -d "compose-files" ]; then
   rm -rf compose-files
fi
mkdir compose-files
mkdir compose-files/orderer
mv generator-peer/crypto-config.yaml generator-peer/configtx.yaml compose-files/
mv generator-peer/org* compose-files/
mv generator-peer/templates compose-files/
mv generator-peer/docker-compose-orderer.yaml compose-files/orderer
