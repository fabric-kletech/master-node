#!/bin/bash
source /tmp/multi/env.sh
NODE=$1
NODE_PATH="FABRIC_CFG_PATH$NODE"

if [ -d "${!NODE_PATH}/crypto-config" ]; then
   rm -R ${!NODE_PATH}/crypto-config
fi

if [ -d "${!NODE_PATH}/channel-artifacts" ]; then
   rm -R ${!NODE_PATH}/channel-artifacts
fi

if [ $NODE -eq 1 ]; then
  mkdir ${!NODE_PATH}/channel-artifacts
fi
